from board import board

def solve(board):

    find = get_empty(board)
    if not find:
        return True
    else:
        row, col = find
    # go trough the board and recursively find solution using backtracking
    for i in range(1, 10):
        if valid_move(board, i, (row, col)):
            board[row][col] = i
            if solve(board):
                return True
            board[row][col] = 0
    return False


def valid_move(board, number, position):

    # row check
    for i in range(len(board[0])):
        if board[position[0]][i] == number and position[1] != i:
            return False

    #column check
    for i in range(len(board)):
        if board[i][position[1]] == number and position[0] != i:
            return False

    # box check
    box_x = position[1] // 3
    box_y = position[0] // 3

    for i in range(box_y * 3, box_y*3 + 3):
        for j in range(box_x * 3, box_x * 3 + 3):
            if board[i][j] == number and (i,j) != position:
                return False

    return True



# print board in formatted form
def print_board(board):
    for i in range(len(board)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - - -")
        for j in range(len(board)):
            if j % 3 == 0:
                print("|",end="")
            print(f" {board[i][j]} ",end="")
        print("|")

# find empty value and return
def get_empty(board):
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j] == 0:
                return i, j
    return None


if __name__ == '__main__':
    print_board(board)
    solve(board)
    print("------------SOLVED---------------")
    print_board(board)
